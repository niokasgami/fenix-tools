


/**
 * find the current active plugin and return the parsed parameters.
 *
 * @function convertParameters
 * @since 2.0.0
 * @memberOf module:Utils
 * @template T - the object struct
 * @returns {any} the current active plugin parsed plugins
 *
 * @example
 * let params = convertParameters<MyPluginParams>();
 */
export function convertParameters<T>(): T {
  const scriptElement = document.currentScript as HTMLScriptElement;
  const currentScript = scriptElement.src.match(/.+\/(.+)\.js/)[1];
  return deepParseJSON(PluginManager.parameters(currentScript)) as T;
}




/**
 * Recursive method that will convert all string values in an object to a more
 * appropriate type.
 *
 * In MV there are a lot of objects filled with strings of different values, a lot
 * of times we need to convert each value manually, instead use this to quickly
 * deep parse each value from string to the correct type.
 *
 * @function deepParseJSON
 * @since 1.0.0
 * @memberof module:Utils
 *
 * @param {object} parameters - The string filled object you want converted
 *
 * @returns An object with it's string values converted
 * @example
 *
 * const myParams = { p1: '22', p2: 'true' }
 * convertParameters(myParams) // => { p1: 22, p2: true }
 *
 * const myParams = { p1: '{a: 1'1, c: '2'}', p2: '[{}, {}, {}]' }
 * convertParameters(myParams) // => { p1: {a: 1, c: 2}, p2: [{}, {}, {}] }
 *
 */
function deepParseJSON (parameters: PluginParameters): unknown {
  function parseParameters (str: string) {
    try {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return JSON.parse(str, (key, value: unknown) => {
        try {
          // eslint-disable-next-line @typescript-eslint/no-unsafe-return
          return parseParameters(value as string)
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
        } catch (_e) {
          return value
        }
      })
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
    } catch (e) {
      return str
    }
  }
  return parseParameters(JSON.stringify(parameters))
}
