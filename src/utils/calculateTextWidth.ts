/* eslint-disable no-control-regex, no-useless-escape */

/**
 * Calculates the visual width of a text string, considering optional message code processing.
 * When processing message codes, text size message codes are ignored and may cause incorrect results.
 *
 * Unsupported message codes:
 * - \I[n] - ignores icon size
 * - \PX[x] - ignores position
 * - \PY[y] - ignores position
 *
 * @param {string} text - The input text to calculate the width for.
 * @param {boolean} [processMessageCodes=false] - If true, skips RPG Maker message codes while calculating width.
 * @returns {number} The calculated visual width of the text.
 */
export  function calculateTextWidth (text: string, processMessageCodes = false): number {
  if (processMessageCodes) {
    // convert RPG Maker codes and then proceed to calculate width
    text = text.replace(/\\/g, '\x1b')
    text = text.replace(/\x1b\x1b/g, '\\')
    while (text.match(/\x1bV\[(\d+)\]/gi)) {
      text = text.replace(/\x1bV\[(\d+)\]/gi, (_, p1: string) => {
            const value = $gameVariables.value(parseInt(p1));
            return value.toString();
          }
      )
    }

    text = text.replace(/\x1bN\[(\d+)\]/gi, (_, p1: string) =>
        Window_Base.prototype.actorName(parseInt(p1))
    )

    text = text.replace(/\x1bP\[(\d+)\]/gi, (_, p1: string) =>
        Window_Base.prototype.partyMemberName(parseInt(p1))
    )

    text = text.replace(/\x1bG/gi, TextManager.currencyUnit)
    text = text.replace(/\x1bC\[(\d+)\]/gi, '') // remove color codes
    text = text.replace(/\x1bI\[(\d+)\]/gi, '') // remove icon code
    text = text.replace(/\x1bPX\[(\d+)\]/gi, '') // remove position code
    text = text.replace(/\x1bPY\[(\d+)\]/gi, '') // remove position code
    text = text.replace(/\x1b\^/gi, '') // remove don't wait for input code
    text = text.replace(/\x1b</gi, '') // remove display all text at once code
    text = text.replace(/\x1b>/gi, '') // remove cancel display all text at once code
    text = text.replace(/\x1b!/gi, '') // remove wait for button input code
    text = text.replace(/\x1b\|/gi, '') // remove wait for 1 second input code
    text = text.replace(/\x1b\./gi, '') // remove wait for 1/4 second input code
    text = text.replace(/\x1b\$/gi, '') // remove open gold code
    text = text.replace(/\x1b\}/gi, '') // remove decrease text size code
    text = text.replace(/\x1b\{/gi, '') // remove increase text size code
    text = text.replace(/\x1bFS\[(\d+)\]/gi, '') // remove change text size to n code
  }

  return [...text].reduce((width, char) => {
    width += (char.charCodeAt(0) > 255 ? 2 : 1)
    return width
  }, 0)
}
