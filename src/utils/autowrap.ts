import {calculateTextWidth} from './calculateTextWidth'

export function autowrap (text: string, maxWidth: number, processMessageCodes = false): string[] {
  const lines: string[] = []

  text.split('\n').forEach(line => {
    if (calculateTextWidth(line, processMessageCodes) <= maxWidth) {
      lines.push(line)
    } else {
      let cur = 0
      let tmpLine = ''

      while (cur < line.length) {
        let word = '' + line[cur++]
        if (word[0] !== ' ' && word.charCodeAt(0) < 256) {
          while (cur < line.length && line[cur] !== ' ' && line.charCodeAt(cur) < 256) {
            word += line[cur++]
          }
        }
        const wordWidth = calculateTextWidth(word, processMessageCodes)
        let tempLineWidth = calculateTextWidth(tmpLine, processMessageCodes)

        if (wordWidth > maxWidth) {
          let tmpWord = ''
          for (const char of word) {
            const tmpWidth = calculateTextWidth(tmpWord + char, processMessageCodes)
            if (tmpWidth + tempLineWidth > maxWidth) {
              lines.push(tmpLine.trim())
              tmpLine = tmpWord = char
              tempLineWidth = 0
            } else {
              tmpWord += char
            }
          }
          tmpLine = tmpWord
        } else {
          if (wordWidth + tempLineWidth > maxWidth) {
            lines.push(tmpLine.trim())
            tmpLine = (word[0] !== ' ' || word.length > 1) ? word : ''
          } else {
            tmpLine += word
          }
        }
      }

      if (tmpLine.length > 0) {
        lines.push(tmpLine.trim())
      }
    }
  })

  return lines
}
