
/**
 * Scan all events on map and extract their comments. This can only be used when
 * the map data is available upon map load. It starts by looping through all events
 * in the current map and through each event page storing all comments.
 *
 * @function loadEventComments
 * @since 1.0.0
 * @memberof module:Utils
 *
 * @return {object} An object of all comments added together, sorted by eventId
 *
 * @example
 * const mapEventComments = loadEventComments()
 * // =>  *  // { 28: [param1, param2, param3] }
 *           // { 29: [param1, param2, param3] }
 *
 *
 */
export function loadEventComments() {
  const allEvents = $dataMap.events
  const meta = {}

  allEvents.filter(event => event)
      .forEach(event => {
        const pages = event.pages
        const eventId = event.id
        const pageComments = []
        pages.forEach((page, index) => {
          const pageId = pages.indexOf(pages[index])
          if (pageId >= -1) {
            let comments = ''
            page.list.forEach(command => {
              if (command.code === 108 || command.code === 408) {
                // eslint-disable-next-line @typescript-eslint/restrict-plus-operands,@typescript-eslint/no-base-to-string
                comments += command.parameters[0]
              }
            })
            if (comments) { pageComments.push(comments) }
          }
        })
        if (pageComments.length > 0) { meta[eventId] = pageComments }
      })
  return meta
}
