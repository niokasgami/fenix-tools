import filterText from './filterText'
/**
 * Finds and extracts a notetag from a multiline string of text and returns it's value
 *
 * @function getMultiLineTag
 * @since 1.0.0
 * @memberof module:Utils
 *
 * @param {string} text - The text to evaluate
 * @param {string} tag - The tag to search for in the text
 *
 * @returns {array} - An array of matches containing the values between the tags
 * @example
 * import { getMultiLineTag } from 'fenix-tools'
 *
 * // $dataWeapons[1].note = '<myTag> opt1: value, opt2: value </myTag>'
 *
 * const myNotes = getMultiLineTag($dataWeapons[1].note, 'myTag') // => ['opt1: value, opt2: value']
 *
 */
export function getMultiLineTag (text: string, tag: string): unknown[] {
  const result = []
  const re = /<([^<>]+)>([\s\S]*?)<(\/[^<>]+)>/g
  // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
  const matches = filterText(text, re, (match) => match[1].toLowerCase() === tag.toLowerCase())
  matches.forEach(group => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
    result.push(group[2].trim())
  })
  return result
}
