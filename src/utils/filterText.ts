/**
 * Uses regex to recursively filter a string.
 *
 * @function filterText
 * @since 1.0.0
 * @memberof module:Utils
 *
 * @param {string} text - The text you would like to filter
 * @param {regex} regex - The regex pattern to use for filtering
 * @param {function} action - The callback function to evaluate
 *
 * @returns {array} An array of groups that match the evaluation
 * @example
 * import {filterText} from 'fenix-tools'
 *
 * const re = /pattern/g
 * const matches = filterText(text, re, (match) => {
 *  console.log(match) // => The full match group returned from regex
 *  // Perform an evaluation to store specific matches
 * })
 *
 */
// eslint-disable-next-line @typescript-eslint/no-unsafe-function-type
export default function filterText (text: string, regex: RegExp, action: Function) {
  const result = []
  let match
  while (match = regex.exec(text)) { // eslint-disable-line no-cond-assign

    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    if (action(match)) {
      result.push(match)
    }
  }
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return result
}
