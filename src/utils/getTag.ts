
import filterText from './filterText'
/**
 * Finds and extracts a notetag from a string of text and returns it's values.
 * Not much different from using {@link module:Utils.getMetaData|getMetaData}, but
 * if you need more control over the string then use this method.
 *
 * @function getTag
 * @since 1.0.0
 * @memberof module:Utils
 *
 * @param {string} text - The text to be evaluated
 * @param {string} tag - The tag to search for in the text
 *
 * @returns {Array} - An array of parameters of the values within the tag <tag: value, value>
 * @example
 * import { getTag } from 'fenix-tools'
 *
 * // $dataActors[1].note = '<myTag: value, value2, value3>'
 *
 * const myTag = getTag($dataActors[1].note, 'myTag') // => 'value, value2, value3'
 *
 *
 */
export function getTag (text: string, tag: string): unknown[] {
  if (!text || !tag) { return }
  const result = []
  const re = /<([^<>:]+)(:?)([^>]*)>/g
  // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
  const matches = filterText(text, re, (match) => match[1].toLowerCase() === tag.toLowerCase())
  matches.forEach(group => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
    result.push(group[3].trim())
  })
  return result
}
