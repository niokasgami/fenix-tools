
/**
 * Retrieves meta values from an RPG Maker MV object that contains the meta property.
 * Will work with any object that contains a meta property, like $dataWeapons,
 * $dataItems, etc
 *
 * @function getMetaData
 * @since 1.0.0
 * @memberof module:Utils
 *
 * @param  {object} obj - The meta object you want to search through.
 * @param {string}  tag - The meta tag you want to search for.
 *
 * @returns {string} The value(s) of the notetag.
 * // TODO : change the example???
 * @example
 * import { getMetaData } from 'fenix-tools'
 *
 * // $dataActors[1].meta = {myTag: 'myTagValue'}
 *
 * const data = $dataActors[1]
 * const meta = getMetaData(data, 'myTag') // => 'myTagValue'
 *
 */
export function getMetaData(obj: object, tag: string): string {
  const meta: unknown = obj[tag];
  const match = Object.keys(meta).filter(key => key.toLowerCase() === tag.toLowerCase());
  const value: unknown = meta[match[0]];
  return typeof value === 'string' ? value : '';
}
