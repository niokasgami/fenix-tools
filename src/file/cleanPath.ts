/**
 * Cleans the given path by replacing two forward slashes with one.
 *
 * @function cleanPath
 * @since 1.0.0
 * @memberOf module:File
 *
 * @param {string} path - The path to clean.
 * @return {string} the clean path.
 * @example
 * import{cleanPath} from "@fenixEngine/tools";
 *
 * const badUrl = 'C://Path//to/something//'
 *
 * console.log(cleanPath(badUrl)) // => 'C:/Path/to/something/'
 */
export function cleanPath(path: string): string {
  return path.replace(/(\/)[(/)]/g, '/');
}
