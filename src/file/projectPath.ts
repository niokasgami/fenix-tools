
/**
 * Gets the location of the current game.exe, a full root project path.
 *
 * @function projectPath
 * @since 1.0.0
 * @memberof module:File
 *
 * @return {string} The current project root path
 * @example
 * import {projectPath} from 'fenix-tools'
 *
 * projectPath() // => 'C:/fullpath/to/project'
 *
 */
export function projectPath(): string {
  let path = window.location.pathname.replace(/\/[^/]*$/, '/');
  if (path.match(/^\/([A-Z]:)/)) {
    path = path.slice(1)
  }
  return decodeURIComponent(path)
}
