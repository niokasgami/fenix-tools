import {hasWebAccess} from "./hasWebAccess";
import url from "url";
import https0 from "https";
import http from "http";

/**
 * An async function which downloads a file via node's http or https.
 *
 * @function downloadFile
 * @async
 * @since 1.0.0
 * @memberof module:File
 * @see {@link module:File.hasWebAccess|hasWebAccess}
 *
 * @param {ConfigOptions} config - A configuration object.
 * @param {string}  config.url - The url to the file you want to download
 * @param {number}  [config.port=443] - The port number to use
 * @param {object}  [config.agent=http.globalAgent] - How much gold the party starts with.
 *
 * @returns {Promise} - Returns a promise that resolves the file's data
 * from the url
 * @example
 * import {downloadFile, tryWriteFile} from 'fenix-tools'
 *
 * downloadFile({
 *  url: 'http://fenixenginemv.gitlab.io/img/fenix-logo-signature.png'
 * port: 80,
 * })
 * .then(data => {
 *   console.log(data) // => The downloaded file's data
 *   tryWriteFile('/', data) // => writes file to machine
 * })
 *
 */
export async function downloadFile(config = {} as ConfigOptions): Promise<unknown> {
  if (!config.url) return new Error('url is required');

  await hasWebAccess();
  const downloadUrl = url.parse(config.url);
  const https = downloadUrl.protocol === 'https:' ? https0 : http;
  const httpsConfig = {
    hostname: downloadUrl.hostname,
    port: config.port || 443,
    path: downloadUrl.href,
    protocol: downloadUrl.protocol,
    agent: config.agent || https.globalAgent
  }

  return await new Promise((resolve, reject) => {
    https.get(httpsConfig, (response) => {
      const chunks: unknown[] = [];
      const status = response.statusCode;
      if (status !== 200) {
        throw new Error(`Failed to load, response status code is ${status}`);

      }
      response.on('data', (chunk) => chunks.push(chunk))
      response.on('end', () => resolve(chunks.join('')))
      response.on('error', (err) => reject(err))
    });
  });
}


interface ConfigOptions {
  url: string;
  port?: number;
  agent: http.Agent | https0.Agent;
}
