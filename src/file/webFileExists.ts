
/**
 * An async function for checking if a file exist on a server.
 *
 * @function webFileExists
 * @async
 * @since 1.0.0
 * @memberof module:File
 *
 * @param {string} url - The url to the file
 *
 * @returns {promise} Promise that resolves to true if the file exists.
 * @example
 * import { webFileExists } from 'fenix-tools'
 *
 * const file = 'http://example.com/file.png'
 *  const fileExists = await webFileExists(file)
 * console.log(fileExists)  // => returns true if file exists
 * // or
 * webFileExists(file)
 * .then(console.log('File exists!'))
 * .catch(console.log('Unable to get file))
 */
export async function webFileExists(url: string) {
  try {
    const result = fetch(url, {
      method: 'HEAD',
      cache: 'no-cache'
    }).then((response) => {
      return response.ok
    })
    return result
  } catch (error) {
    throw new Error(error as string | undefined);
  }
}
