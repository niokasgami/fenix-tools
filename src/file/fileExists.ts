import {localFileExists} from "./localFileExists";
import {webFileExists} from "./webFileExists";

/**
 * Check if a file exists.
 *
 * @function fileExists
 * @since 1.0.0
 * @memberof module:File
 * @see {@link module:File.localFileExists|localFileExists}
 * @see {@link module:File.webFileExists|webFileExists}
 *
 * @param {string} url - The path to the file
 *
 * @returns {Boolean} true if the file exists.
 * @example
 * import { fileExists } from 'fenix-tools'
 *
 * console.log(fileExists('./path/to/file')) // => returns true or false
 *
 */
export function fileExists(url: string): boolean {
  if(Utils.isNwjs()) {
    return localFileExists(url);
  }
  else {
    let result = false;

    void webFileExists(url).then(exists => {
      result = exists;
    });
    return result;
  }
}
