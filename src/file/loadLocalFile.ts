import fs from "fs";


/**
 * Load a file on the local machine.
 *
 * @function loadLocalFile
 * @since 1.0.0
 * @memberof module:File
 *
 * @param {string} path - Path to the file.
 * @param {string} [_encoding='utf8'] - The type of file encoding
 *
 * @return {promise} A promise that resolves the data
 * @example
 * import {loadLocalFile} from 'fenix-tools'
 *
 * loadLocalFile('./img/pictures/character1.png)
 * .then(data => {
 *  // Local file loaded success
 *  console.log(data)  // => A parsed JSON object.
 * })
 *
 */
export  function loadLocalFile (path: string, _encoding = 'utf8') {
  return new Promise((resolve, reject) => {
    if (fs.existsSync(path)) {
      const options = {encoding: _encoding as BufferEncoding}
      const contents = fs.readFileSync(path, options);
      resolve(contents)
    }
    reject(new Error(`Cannot read file at ${path}`))
  })
}
