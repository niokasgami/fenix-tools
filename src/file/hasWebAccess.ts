/**
 * An async function which determine if the current user has internet access by
 * pinging to a server. If no url is provided, it checks navigator.online
 *
 * @function hasWebAccess
 * @async
 * @since 1.0.0
 * @memberOf module:File
 *
 * @param {string} [url] - A url to a server
 *
 * @return {Promise<boolean>} return a promise whether the user has access to internet or no.
 * @example
 * import {hasWebAccess} from "@fenixEngine/tools"
 * const canDownload = await hasWebAccess("http://google.com")
 * console.log(canDownload);
 *
 * hasWebAcess("http://google.com").then("Web connection is live!");
 */
export async function hasWebAccess(url?: string): Promise<boolean> {
  if(!url) return navigator.onLine;
  return await fetch(url, {
    method: 'HEAD',
    cache: 'no-cache'
  }).then((response) => {
    return response.ok;
  });
}
