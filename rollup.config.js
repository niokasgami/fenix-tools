import eslint from 'rollup-plugin-eslint-bundle'

export default {
  input: './src/index.js',
  output: [{
    file: './lib/fenix-tools-es.js',
    name: 'FenixTools',
    format: 'es',
    globals: {},
    intro: `'use strict'`,
    indent: false,
    external: ['fs', 'http']
  },
  {
    file: './lib/fenix-tools.js',
    name: 'FenixTools',
    format: 'cjs',
    globals: {},
    indent: false,
    external: ['fs', 'http']
  }
  ],
  plugins: [
    eslint({
      exclude: 'node_modules/**',
      useEslintrc: true,
      fix: true,
      include: [
        './lib/fenix-tools-lib.js',
        './lib/fenix-tools-es.js',
        './lib/fenix-tools.js'
      ]
    })
  ]
}
