#  fenix-tools | FeniX Engine Developer Tools
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![pipeline status](https://gitlab.com/FeniXEngineMV/fenix-tools/badges/dev/pipeline.svg)](https://gitlab.com/FeniXEngineMV/fenix-tools/commits/dev)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

<a href='https://ko-fi.com/ltngames' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi2.png?v=0' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>


FeniXTools is a modular library built for use with RPG Maker MV plugin development.

Fenix Tools is designed with modern ES6 Javascript and is was created to help with common tasks which occur often when developing plugins for RPG Maker MV.

### Tree-shakeable!

When using a bundler like Webpack or Rollup for bundling plugins, you can ensure you get only what you use in your plugin and not the whole library with it. This makes your plugins self reliant and it won't depend on the entire fenix-tools library to be used.

# Installation
### Prerequisites
 _Skip if you already have NodeJS & npm._

#### Windows

- Install [NodeJS](https://nodejs.org/en/)

#### Ubuntu

- Install [NodeJS](https://nodejs.org/en/)

```
sudo apt-get install nodejs
```

- Install [npm](https://www.npmjs.com/)

```
sudo apt-get install npm
```

-------------------------------
### Install fenix-tools

```
npm install fenix-tools --save-dev 
```

or

Have a look at our tags page for current [releases]([https://gitlab.com/FeniXEngineMV/fenix-tools/tags)

# Getting Started

❗ If you would rather an easy to use command line tool, then check out
*[Wizard](https://gitlab.com/FeniXEngineMV/wizard), a CLI tool that generates and builds projects for you*

❗ If you would rather learn by example then have a look at [fenix-tools-example](https://gitlab.com/FeniXEngineMV/fenix-tools-example) for an easy start.

### Basics
Before you can use FeniXTools you must have your environment setup to use ES6 modules. To start we will need a bundler library, the bundler library is responsible for bundling the code into one file and correctly interpreting `import` and `export` for you.

In this guide you will be shown 3 steps for setting up your project and building your plugins using FeniXTools as your helper library.

1. Install & Setup RollupJS
2. Setup directory structure
2. Import FeniXTools methods
3. Build your files

### Install & Setup RollupJS

Install rollup and it's plugin for resolving node dependencies

```
npm install rollup rollup-plugin-node-resolve --save-dev
```

Create a `rollup.config.js` file

```js
import resolve from 'rollup-plugin-node-resolve'

export default {
  input: `./src/example-plugin/main.js`,
  external: [
    'fs-extra',
    'http',
    'path'
  ],
  output: [{
    file: `./game/js/plugins/example-plugin.js`,
    format: 'iife',
    indent: false
  }
  ],
  plugins: [
    resolve({
      jsnext: true,
      module: true
    }
    )
  ]
}
```

### Setup directory structure

This structure is for developing more than one plugin per project and every plugin contains it's own folder within
the `src` directory.

```
├── game 
├── src  
│  ├── new-plugin
│  │   ├── Core.js
│  │   ├── main.js
│  │   ├── Parameters.js
│  ├── plugin-two
│  ├── plugin-three
├── test
├── LICENSE
├── README.md
└── package.json
```

Every plugin will require a `main.js` entry point file, which is used by RollupJS to bundle all the code and create the single plugin file.

### Import FeniXTools library

🗒️ *All FeniXEngine projects contain a `Core.js` and a `Parameters.js` file. You are not required to name your files this unless you are creating plugins for FeniXEngine plugin suite or you are using FeniXCLI.*

#### Include the entire FeniXTools library

```js
// Core.js
import * as FeniXTools from 'fenix-tools'
```

#### OR simply import the method you would like to use in your code.

```js
import {loadLocalFile} from 'fenix-tools'
```

### Build your plugin(s)

In your `main.js` file you will need to let RollupJS know what to include in the final bundle. Most plugins you will be simply exporting every single file in your plugin's folder.

```js
/* eslint-disable */
export * from './Parameters'
export * from './Core'
```

Now run RollupJS which will create your plugin to the output directory in your `rollup.config.js` which is currently set to `./game/js/plugins/`

```
npx rollup -c
```

If you installed Rollup globally you can run rollup directly

```
rollup -c
```

# Documentation

A good place to get started is the documentation page for a list of available methods.

[Documentation](https://fenixenginemv.gitlab.io/fenix-tools/)


# Want To Contribute ?
It's really easy to get started with Fenix Tools development

### Forking And Cloning

- [Fork](https://gitlab.com/FeniXEngineMV/fenix-tools/forks/new) the repo to your own account.

- Clone the repository to a directory on your machine, where `UserName` is your gitlab username

 `git clone git@gitlab.com:UserName/plugins.git`

For a basic how-to on cloning read this [Basic git commands](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html)

--------------------
### Install Required Packages

Once cloned, change to the root directory of the project and open the command line/terminal and install the packages required for FeniX plugin development

 `npm install`

Once installation is complete, we use RollupJS to bundle all files.

`npm run build`

The resulting output after bundling is placed in `./lib/` folder.

# Credits

 While this is open source and credit is not required, it would be appreciated it you linked to FeniX Engine's website or GitLab repository to show support.

https://fenixenginemv.gitlab.io/

All plugins and tools developed for FeniX engine must retain its license information in the header of all plugins and files and must not be removed.

# Contributing
Feel free to contribute and help us out, start by creating an [issue](https://gitlab.com/FeniXEngineMV/fenix-tools/-/issues/new).

# License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[The MIT License](https://gitlab.com/FeniXEngineMV/fenix-tools/-/blob/main/LICENSE)