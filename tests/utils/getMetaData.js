import test from 'ava'
import { getMetaData } from '../../src'

// getMetaData
test('getMetaData', async t => {
  const $dataActors = {
    meta: {
      cat: 'string',
      meta: 'meta'
    }
  }
  await t.deepEqual(getMetaData($dataActors, 'Cat'), 'string', 'message')
})
