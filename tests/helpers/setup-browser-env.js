import browserEnv from 'browser-env'
import fetch from 'node-fetch'

const actors = [
  null,
  {
    name () {
      return 'Harold'
    }
  }
]

const variables = [
  null,
  '20'
]

browserEnv(['window', 'document', 'navigator'])
global.window.fetch = fetch
global.window.Utils = {}
global.window.Utils.isNwjs = () => true
global.window.Window_Base = {
  prototype: {
    actorName (n) {
      const actor = n >= 1 ? window.$gameActors.actor(n) : null
      return actor ? actor.name() : ''
    },
    partyMemberName (n) {
      // default rpg maker uses [n - 1] but for mocking purposes we use [n]
      const actor = n >= 1 ? window.$gameParty.members()[n] : null
      return actor ? actor.name() : ''
    }
  }
}

global.window.$gameVariables = {
  value (n) {
    return variables[n]
  }
}

global.window.$gameActors = {
  actor (id) {
    return actors[id]
  }
}

global.window.$gameParty = {
  members () {
    return actors
  }
}

global.window.TextManager = {
  currencyUnit () {
    return 'JPY'
  }
}
