import test from 'ava'
import { webFileExists } from '../../src/index'

test('webFileExists - test for a web file that exists', async t => {
  const webUrl = 'https://gitlab.com/FeniXEngineMV/plugins/blob/develop/src/fenix-core/Core.js'
  const result = await webFileExists(webUrl)
  t.true(result, 'Web file must exist!')
})

test('webFileExists - test for a web file that does not exist', async t => {
  const webUrl = 'https://gitlab.com/FeniXEngineMV/plugins/blob/dev/src/NotAFile.js'
  const result = await webFileExists(webUrl)
  t.false(result, 'Web file must not exist!')
})
