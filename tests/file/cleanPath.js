import test from 'ava'

import { cleanPath } from '../../src/index'

test('cleanPath', t => {
  t.is(cleanPath('//'), '/', 'Clean double forward slash into into one')
})
