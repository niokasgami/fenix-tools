import test from 'ava'
import { loadJSON } from '../../src/index'

test('loadJSON - test that a local file exists', async t => {
  await t.notThrows(() => {
    loadJSON('./package.json')
  })
})
