import test from 'ava'
import { localFileExists } from '../../src/index'

test('localFileExists - test argument as array', t => {
  t.truthy(localFileExists(['./package.json', './package.json']))
  t.falsy(localFileExists(['.//notAFile.json', './/notAFile.json']))
})

test('localFileExists - test argument as single string', t => {
  t.truthy(localFileExists('./package.json'))
  t.falsy(localFileExists('.//notAFile.json'))
})