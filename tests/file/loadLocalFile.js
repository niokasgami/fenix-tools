import test from 'ava'
import { loadLocalFile } from '../../src/index'

test('loadLocalFile - test loading a local file', async t => {
  const file = await loadLocalFile(`./tests/helpers/TestFile.md`)
  t.notThrows(() => file, 'Must resolve')
})

test('loadLocalFile - test data contents of loaded file', async t => {
  const file = loadLocalFile('./tests/helpers/TestFile.md')
  t.snapshot(file)
  await loadLocalFile(`./tests/helpers/TestFile.md`)
    .then(data => {
      t.snapshot(data)
    })
})

test('loadLocalFile - test throws when no file exists', async t => {
  await t.throws(loadLocalFile(`./notAFile`))
})
