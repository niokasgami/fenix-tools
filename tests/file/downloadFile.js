import test from 'ava'
import { downloadFile } from '../../src/index'

test('downloadFile - test the data downloaded', async t => {
  const config = {
    url: 'https://gitlab.com/FeniXEngineMV/fenix-tools/raw/dev/README.md',
    destination: './tests/dumps/FNX_Core.js'
  }
  const fileData = await downloadFile(config)
  t.snapshot(fileData, 'Should be chunks')
})
